EDITOR=vim

# suckless
function rdwm() {
  OLDPWD="$(pwd)"
  cd $HOME/gitlab/apollo/dwm-flexipatch

  $EDITOR -p config.def.h patches.def.h

  if ! cmp config.def.h config.h >/dev/null 2>&1; then
    rm -f config.h patches.hi
    make
    sudo make install clean
  fi

  if ! cmp patches.def.h patches.h >/dev/null 2>&1; then
    rm -f config.h patches.h
    make
    sudo make install clean
  fi

  cd "$OLDPWD"
}

function rdmenu() {
  OLDPWD="$(pwd)"
  cd $HOME/gitlab/apollo/dmenu

  $EDITOR config.def.h

  if ! cmp config.def.h config.h >/dev/null 2>&1; then
    rm -f config.h
    make
    sudo make install clean
  fi

  cd "$OLDPWD"
}

function rst() {
  OLDPWD="$(pwd)"
  cd $HOME/gitlab/apollo/st

  $EDITOR config.def.h

  if ! cmp config.def.h config.h >/dev/null 2>&1; then
    rm -f config.h
    make
    sudo make install clean
  fi

  cd "$OLDPWD"
}

function rslock() {
  OLDPWD="$(pwd)"
  cd $HOME/gitlab/apollo/slock

  $EDITOR config.def.h

  if ! cmp config.def.h config.h >/dev/null 2>&1; then
    rm -f config.h
    make
    sudo make install clean
  fi

  cd "$OLDPWD"
}

function rdwmblocks() {
  OLDPWD="$(pwd)"
  cd $HOME/gitlab/apollo/dwmblocks

  $EDITOR blocks.def.h

  if ! cmp blocks.def.h blocks.h >/dev/null 2>&1; then
    rm -f blocks.h
    make
    sudo make install clean
    pkill -USR1 dwmblocks
  fi

  cd "$OLDPWD"
}

# git
function commit() {
  icon=":robot_face:"
  upstream="$(git config --get remote.origin.url)"
  test "${string#*"github"}" != "$upstream" && icon="🤖"

    #[ -f .commit ] && msg="$(cat .commit)" || msg="Commit automático"
    [ -f .commit ] && msg="$icon $(cat .commit)" || msg="$icon Alterações: $(git status -s -z)"
    [ $1 ] && msg="$@"
    git add .
    git commit -m "$msg"
    git push
}

