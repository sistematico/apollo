#!/usr/bin/env bash

#LOCK=${LOCK:-$(xset q | awk '/timeout:/{print $2}')}
#SUSPEND=${SUSPEND:-$(xset q | awk '/Standby:/{print $4}')}

PARAMS="--not-when-audio" # --not-when-fullscreen
LOCK=1     # Minutes
SUSPEND=60 # Minutes
MLOCK=$(( LOCK * 60 ))
MSUSPEND=$(( SUSPEND * 60 ))
CSUSPEND=$(( MSUSPEND - MLOCK ))

echo $MSUSPEND $CSUSPEND $MLOCK

#xidlehook --not-when-audio --timer $MLOCK 'slock' '' --timer $CSUSPEND 'sudo systemctl poweroff' ''
# Only exported variables can be used within the timer's command.
export PRIMARY_DISPLAY="$(xrandr | awk '/ primary/{print $1}')"

# Run xidlehook
xidlehook \
  `# Don't lock when there's a fullscreen application` \
  --not-when-fullscreen \
  `# Don't lock when there's audio playing` \
  --not-when-audio \
  `# Dim the screen after 60 seconds, undim if user becomes active` \
  --timer 60 \
    'xrandr --output "$PRIMARY_DISPLAY" --brightness .1' \
    'xrandr --output "$PRIMARY_DISPLAY" --brightness 1' \
  `# Undim & lock after 10 more seconds` \
  --timer 10 \
    'xrandr --output "$PRIMARY_DISPLAY" --brightness 1; i3lock' \
    '' \
  `# Finally, suspend an hour after it locks` \
  --timer 3600 \
    'systemctl suspend' \
    ''
