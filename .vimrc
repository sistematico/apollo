set autoindent 
set expandtab       " Usa espaços em vez de tabs
set tabstop=2       " Número de colunas que um tab conta
set shiftwidth=2    " Número de espaços para indentação
set softtabstop=2   " Número de espaços de um tab quando o 'expandtab' está ativo

