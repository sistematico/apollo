# Base
alias e='exit'
alias s='sudo su'
alias apt='sudo apt'
alias rehash="source $HOME/.bashrc"

# Misc
alias fd='fdfind -uL'
